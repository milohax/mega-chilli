# mega-chilli

A project for chili recipe varieties (not code).

First published at https://milosophical.me/pg/unforgetable-mega-chilli.html on 2009-01-09 08:03:34 UTC+11:00

The idea is to have forks/branches of this project with different varieties, such as "Unforgettable" (with rosemary), or whisky, ginger, maybe sweet varieties?

See the Branches and Tags on this project.

Experiments will be chronicled in the Issues.
